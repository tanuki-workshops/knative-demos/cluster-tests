
//String.prototype.trimIndent = function() { return this.split("\n").map(item => item.trim()).join("\n") }
var counter = 0

function metrics() {
  let results = `
    # HELP small_app_counter: call counter function
    # TYPE small_app_counter gauge
    small_app_counter ${counter}
    # HELP other_app_counter: call counter function
    # TYPE other_app_counter counter
    other_app_counter ${counter}  
  `
  return results
}

function index() {
  return `
    <!doctype html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Hello World</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
        .container
        {
          min-height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
        }
        .title
        {
          font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
          display: block;
          font-weight: 300;
          font-size: 100px;
          color: #35495e;
          letter-spacing: 1px;
        }
      </style>
    </head>
    <body>
      <section class="container">
          <h1 class="title">
            👋 hello world 🌍🎃🎃🎃🎃
            <!--👋 hello world 🌍-->
            <!--👋 hello world 🌍 🟣-->
            <!--👋 hello world 🌍 🔵-->
            <!--👋 hello world 🌍 🟢-->
          </h1>
      </section>
    </body>
    </html>
  `
}


function hello() {
  return "👋 Hello World 🌍"
}

function main(params) {
  counter+=1
  return {
    message: hello(),
    total: 42,
    authors: ["@k33g_org"],
    context: "this is a demo 🚀💥💥",
    params: params.getString("name"),
    revision: "nil",
    counter: counter
  }
}

// 🟠🟡🟢🔵🟣🔴⚫️⚪️🟤

