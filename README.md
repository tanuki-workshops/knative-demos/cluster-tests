# cluster-tests

## TODO

- Setup the Kubernetes integration and 🖐️ keep unchecked the "GitLab-managed cluster" checkbox
- Disable the shared runners of GitLab.com

## 🖐️ If you keep unchecked the "GitLab-managed cluster" checkbox

You can manage your own namespaces, and you need some additional steps (see below)

### CI Variables setup (if )

- `CLUSTER_URL` (K8S API url, for example: https://kubernetes.default:443)
- `CLUSTER_NAME` (cluster name, use the given one when setting up the GitLab Kubernetes integration)
- `CERTIFICATE_AUTHORITY_DATA` (🖐️ **type file**) (cluster certificate)
- `CLUSTER_TOKEN` (cluster token)

### CI

```yaml
.cluster-config: &cluster-config
- |
    kubectl config set-cluster ${CLUSTER_NAME} --server="${CLUSTER_URL}"
    # 👋 the type of the variable is `file`
    kubectl config set-cluster ${CLUSTER_NAME} --certificate-authority="${CERTIFICATE_AUTHORITY_DATA}"
    kubectl config set-credentials gitlab --token="${CLUSTER_TOKEN}"
    kubectl config set-context default --cluster=${CLUSTER_NAME} --user=gitlab
    kubectl config use-context default
```
